=== Plugin Name ===
Contributors: laborin
Donate link: https://laborin.com.mx
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Super simple plugin coded as a test project.

== Description ==

Super simple plugin coded as a test project.

== Installation ==

== Frequently Asked Questions ==

== Screenshots ==


== Changelog ==

= 1.0 =
* Initial -and unique- version


