(function( $ ) {
	'use strict';

	$(".emmanuels-lead-generator-form").submit( function( ev ){

		ev.preventDefault();
		var $form = $( this );
		var $overlay = $form.find( ".overlay" );
		$overlay.html( emmanuels_lead_generator_vars.texts.holdon );
		$overlay.addClass( "active" );

		// I'm too lazy to add field validations, so let's the browser deal with that
		// however, we are sanitizing the input anyways.

		// The API KEY should be retrieved from a site meta and managed through a settings page
		// but if I continue adding stuff I'll never finish this thing
		$.getJSON( "https://api.timezonedb.com/v2/get-time-zone?key=OTHGI70NUXSE&format=json&by=zone&zone=America/Hermosillo", function( data ) {

			$form.find( 'input[name="current_time"]' ).val( data.formatted );

			$.post( emmanuels_lead_generator_vars.ajax_url, $form.serializeArray(), function( response ) {

				$overlay.html( response.message );

			}, "json" ).fail( function(){

				$overlay.html( emmanuels_lead_generator_vars.texts.error );

			} );

		}).fail(function() {

			$overlay.html( emmanuels_lead_generator_vars.texts.error );

		})



	} );

})( jQuery );
