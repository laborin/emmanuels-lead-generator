<form class="emmanuels-lead-generator-form">
	<?php $tmpid = md5(uniqid(rand(), true)); // Using unique ID's so we can use the shortcode in different places in the same page if needed ?>
	<label for="name_<?php echo $tmpid; ?>"><?php echo $atts["name"];?>
		<input type="text" id="name_<?php echo $tmpid; ?>" maxlength="<?php echo $atts["name_maxlength"];?>" name="name" required/>
	</label>
	<?php $tmpid = md5(uniqid(rand(), true)); ?>
	<label for="phone_<?php echo $tmpid; ?>"><?php echo $atts["phone"];?>
		<input type="text" id="phone_<?php echo $tmpid; ?>" maxlength="<?php echo $atts["phone_maxlength"];?>" name="phone" required/>
	</label>
	<?php $tmpid = md5(uniqid(rand(), true)); ?>
	<label for="email_<?php echo $tmpid; ?>"><?php echo $atts["email"];?>
		<input type="email" id="email_<?php echo $tmpid; ?>" maxlength="<?php echo $atts["email_maxlength"];?>" name="email" required/>
	</label>
	<?php $tmpid = md5(uniqid(rand(), true)); ?>
	<label for="budget_<?php echo $tmpid; ?>"><?php echo $atts["budget"];?>
		<input type="number" id="budget_<?php echo $tmpid; ?>" maxlength="<?php echo $atts["budget_maxlength"];?>" name="budget" min="10" max="1000000" />
	</label>
	<?php $tmpid = md5(uniqid(rand(), true)); ?>
	<label for="message_<?php echo $tmpid; ?>"><?php echo $atts["message"];?>
		<textarea name="message" id="message_<?php echo $tmpid; ?>" maxlength="<?php echo $atts["message_maxlength"];?>" rows="<?php echo $atts["message_rows"];?>" cols="<?php echo $atts["message_cols"];?>" required></textarea>
	</label>

	<input type="hidden" name="emmanuels-lead-generator-nonce" value="<?php echo wp_create_nonce( 'emmanuels-lead-generator-submit' );?>"/>
	<input type="hidden" name="current_time"/>
	<input type="hidden" name="action" value="emmanuels_lead_generator_submit"/>
	<input type="submit" value="Submit"/>
	<div class="overlay"></div>
</form>
