��    +      t  ;   �      �     �     �     �     �     �     �  	             (     B     Q     _     e     v     �     �  
   �     �     �     �     �     �  	   �               1     B      O      p     �     �     �  J   �  .     ,   D  
   q     |     �     �     �     �     �  �  �     �	     �	     �	     �	     �	     
     
     &
      8
     Y
     m
     |
     �
     �
     �
     �
  	   �
     �
     �
     
               '     5     R     f  	   t     ~     �     �     �     �  *   �  *   �  3   *  
   ^     i     |     �     �     �     �        
                                               (       &                                 +                $                        %   )      "   !              	             #   *             '       Add New Add New Customer All Customers Customer Customer Archives Customer Attributes Customers Customers list Customers list navigation Desired Budget Edit Customer Email Emmanuel Laborin Emmanuel's Lead Generator Featured Image Filter customers list Hold on... Insert into customer Leads captured Message Name New Customer Not found Not found in Trash Oh noes, a haker =0!! Parent Customer: Phone Number Post Type General NameCustomers Post Type Singular NameCustomer Remove featured image Search Customer Set featured image Something bad happened trying to save your data =(. Please try again later Something happened =(, please try again later. Super simple plugin coded as a test project. Thank you! Update Customer Uploaded to this customer Use as featured image View Customer http://laborin.com.mx https://laborin.com.mx Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Emmanuel's Lead Generator
POT-Creation-Date: 2018-02-15 06:47-0700
PO-Revision-Date: 2018-02-15 06:52-0700
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: emmanuels-lead-generator.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: es_MX
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Añadir nuevo Añadir Cliente Nuevo Mostrar todos los clientes Cliente Historial de Clientes Atributos de los clientes Clientes Lista de clientes Navegación de lista de clientes Presupuesto Deseado Editar Cliente Email Emmanuel Laborin Emmanuel's Lead Generator Imagen destacada Filtrar lista de clientes Espera... Insertar en cliente Lista de prospectos Mensaje Nombre Nuevo cliente No encontrado No encontrado en la Papelera Oh no! un jaker =0! Cliente Padre Teléfono Clientes Cliente Eliminar la imagen destacada Buscar Cliente Definir imagen destacada Por favor, inténtelo de nuevo más tarde. Por favor, inténtelo de nuevo más tarde. Plugin super simple creado como proyecto de prueba. ¡Gracias! Actualizar cliente Subido a este cliente Usar como imagen destacada Ver Cliente http://laborin.com.mx https://laborin.com.mx 