<?php

class Emmanuels_Lead_Generator {

	/**
	 * Define the core functionality of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		add_action( 'init', array( $this, 'register_stuff' ), 0 );

	}

	/**
	 * Registers the "Customer" Custom Post Type
	 *
	 * @since    1.0.0
	 */
	function register_stuff() {

		$labels = array(
			'name'                  => _x( 'Customers', 'Post Type General Name', 'emmanuels-lead-generator' ),
			'singular_name'         => _x( 'Customer', 'Post Type Singular Name', 'emmanuels-lead-generator' ),
			'menu_name'             => __( 'Customers', 'emmanuels-lead-generator' ),
			'name_admin_bar'        => __( 'Customer', 'emmanuels-lead-generator' ),
			'archives'              => __( 'Customer Archives', 'emmanuels-lead-generator' ),
			'attributes'            => __( 'Customer Attributes', 'emmanuels-lead-generator' ),
			'parent_item_colon'     => __( 'Parent Customer:', 'emmanuels-lead-generator' ),
			'all_items'             => __( 'All Customers', 'emmanuels-lead-generator' ),
			'add_new_item'          => __( 'Add New Customer', 'emmanuels-lead-generator' ),
			'add_new'               => __( 'Add New', 'emmanuels-lead-generator' ),
			'new_item'              => __( 'New Customer', 'emmanuels-lead-generator' ),
			'edit_item'             => __( 'Edit Customer', 'emmanuels-lead-generator' ),
			'update_item'           => __( 'Update Customer', 'emmanuels-lead-generator' ),
			'view_item'             => __( 'View Customer', 'emmanuels-lead-generator' ),
			'view_items'            => __( 'View Customer', 'emmanuels-lead-generator' ),
			'search_items'          => __( 'Search Customer', 'emmanuels-lead-generator' ),
			'not_found'             => __( 'Not found', 'emmanuels-lead-generator' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'emmanuels-lead-generator' ),
			'featured_image'        => __( 'Featured Image', 'emmanuels-lead-generator' ),
			'set_featured_image'    => __( 'Set featured image', 'emmanuels-lead-generator' ),
			'remove_featured_image' => __( 'Remove featured image', 'emmanuels-lead-generator' ),
			'use_featured_image'    => __( 'Use as featured image', 'emmanuels-lead-generator' ),
			'insert_into_item'      => __( 'Insert into customer', 'emmanuels-lead-generator' ),
			'uploaded_to_this_item' => __( 'Uploaded to this customer', 'emmanuels-lead-generator' ),
			'items_list'            => __( 'Customers list', 'emmanuels-lead-generator' ),
			'items_list_navigation' => __( 'Customers list navigation', 'emmanuels-lead-generator' ),
			'filter_items_list'     => __( 'Filter customers list', 'emmanuels-lead-generator' ),
		);
		$args = array(
			'label'                 => __( 'Customer', 'emmanuels-lead-generator' ),
			'description'           => __( 'Leads captured', 'emmanuels-lead-generator' ),
			'labels'                => $labels,
			'supports'              => array( 'custom-fields' ), // I'm too lazy to add code to show/edit custom fields, so let's WordPress handle it
			'hierarchical'          => false,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-businessman',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => false,
			'capability_type'       => 'page',
		);

		register_post_type( 'customer', $args ); // This is a VERY common name for post_type but it was required that way. This is just a sample plugin though...

		add_filter( 'manage_customer_posts_columns', array( $this, 'set_custom_edit_customer_columns' ) );

		add_action( 'manage_customer_posts_custom_column' , array( $this, 'custom_customer_column' ), 10, 2 );

		add_shortcode( 'emmanuels-lead-generator', array( $this, 'shortcode' ) );

		if ( is_admin() ) {
			add_action( 'wp_ajax_nopriv_emmanuels_lead_generator_submit', array( $this, 'emmanuels_lead_generator_submit' ) );
			add_action( 'wp_ajax_emmanuels_lead_generator_submit', array( $this, 'emmanuels_lead_generator_submit' ) );
		}

		wp_register_script( 'emmanuels-lead-generator-front-script', plugin_dir_url( __FILE__ ) . 'public/js/emmanuels-lead-generator-public.js' , array( 'jquery-core' ), '', true );
		wp_localize_script( 'emmanuels-lead-generator-front-script', 'emmanuels_lead_generator_vars', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'texts' => array(
				'holdon' => __( 'Hold on...', 'emmanuels-lead-generator' ),
				'error' => __( 'Something happened =(, please try again later.', 'emmanuels-lead-generator' )
			)
		) );
		wp_register_style( 'emmanuels-lead-generator-front-style', plugin_dir_url( __FILE__ ) . 'public/css/emmanuels-lead-generator-public.css' );

	}

	/**
	 * Add the custom columns to the customer post type
	 *
	 * @since    1.0.0
	 */
	function set_custom_edit_customer_columns($columns) {
		unset( $columns['title'] );
		$columns['emmanuels_lead_generator_customer_name'] = __( 'Name', 'emmanuels-lead-generator' );
		$columns['emmanuels_lead_generator_customer_phone'] = __( 'Phone Number', 'emmanuels-lead-generator' );
		$columns['emmanuels_lead_generator_customer_email'] = __( 'Email', 'emmanuels-lead-generator' );
		$columns['emmanuels_lead_generator_customer_budget'] = __( 'Desired Budget', 'emmanuels-lead-generator' );
		$columns['emmanuels_lead_generator_customer_message'] = __( 'Message', 'emmanuels-lead-generator' );
		return $columns;
	}

	/**
	 * Add the data to the custom columns for the customer post type
	 *
	 * @since    1.0.0
	 */
	function custom_customer_column( $column, $post_id ) {

		$output = get_post_meta( $post_id , $column , true );

		if( $column == 'emmanuels_lead_generator_customer_budget' ) $output = '$ ' . money_format('%i', $output );
		echo $output;

	}

	/**
	 * Renders the shortcode
	 *
	 * @since    1.0.0
	 */
	function shortcode( $atts ) {

		// Attributes
		$atts = shortcode_atts(
			array(
				'name' => 'Name',
				'phone' => 'Phone Number',
				'email' => 'Email',
				'budget' => 'Desired Budget',
				'message' => 'Message',
				'name_maxlength' => 524288,
				'phone_maxlength' => 524288,
				'email_maxlength' => 524288,
				'budget_maxlength' => 524288,
				'message_maxlength' => 524288,
				'message_rows' => 5,
				'message_cols' => 50,
			),
			$atts,
			'emmanuels-lead-generator'
		);

		// To generate valid HTML attributes
		$atts['name_maxlength'] = intval( $atts['name_maxlength'] );
		$atts['phone_maxlength'] = intval( $atts['phone_maxlength'] );
		$atts['email_maxlength'] = intval( $atts['email_maxlength'] );
		$atts['budget_maxlength'] = intval( $atts['budget_maxlength'] );
		$atts['message_maxlength'] = intval( $atts['message_maxlength'] );
		$atts['message_rows'] = intval( $atts['message_rows'] );
		$atts['message_cols'] = intval( $atts['message_cols'] );

		wp_enqueue_script( 'emmanuels-lead-generator-front-script' );
		wp_enqueue_style( 'emmanuels-lead-generator-front-style' );

		ob_start();
		require plugin_dir_path( __FILE__  ) . 'public/partials/emmanuels-lead-generator-public-display.php';
		$output = ob_get_clean();

		return $output;

	}

	/**
	 * Process AJAX form submission
	 *
	 * @since    1.0.0
	 */
	function emmanuels_lead_generator_submit( ) {

		if( wp_verify_nonce( $_REQUEST['emmanuels-lead-generator-nonce'], 'emmanuels-lead-generator-submit' ) ){

			$name = sanitize_text_field( $_REQUEST['name'] );
			$phone = sanitize_text_field( $_REQUEST['phone'] );
			$email = sanitize_email( $_REQUEST['email'] );
			$budget = sanitize_text_field( $_REQUEST['budget'] );
			$message = sanitize_textarea_field( $_REQUEST['message'] );
			$current_time = sanitize_text_field( $_REQUEST['current_time'] );

			$post_arr = array(
				'post_title'   => sanitize_title( $name ),
				'post_type'    => 'customer',
				'post_status'  => 'publish',
				'meta_input'   => array(
					'emmanuels_lead_generator_customer_name' => $name,
					'emmanuels_lead_generator_customer_phone' => $phone,
					'emmanuels_lead_generator_customer_email' => $email,
					'emmanuels_lead_generator_customer_budget' => $budget,
					'emmanuels_lead_generator_customer_message' => $message,
					'emmanuels_lead_generator_customer_time_from_external_api' => $current_time
				),
			);

			$result = wp_insert_post( $post_arr );

			if( !$result ) {

				wp_send_json( array( "message" => __( 'Something bad happened trying to save your data =(. Please try again later', 'emmanuels-lead-generator' ) ) );

			} else {

				wp_send_json( array( "message" => __( 'Thank you!', 'emmanuels-lead-generator' ) ) );

			}

		} else {

			wp_send_json( array( "message" => __( 'Oh noes, a haker =0!!', 'emmanuels-lead-generator' ) ) );
		}

	}

}
