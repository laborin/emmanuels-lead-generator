<?php

/**
 * @link              http://laborin.com.mx
 * @since             1.0.0
 * @package           Emmanuels_Lead_Generator
 *
 * @wordpress-plugin
 * Plugin Name:       Emmanuel's Lead Generator
 * Plugin URI:        https://laborin.com.mx
 * Description:       Super simple plugin coded as a test project.
 * Version:           1.0.0
 * Author:            Emmanuel Laborin
 * Author URI:        http://laborin.com.mx
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       emmanuels-lead-generator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once plugin_dir_path( __FILE__  ) . 'class-emmanuels-lead-generator.php';

$emmanuels_lead_generator = new Emmanuels_Lead_Generator();
